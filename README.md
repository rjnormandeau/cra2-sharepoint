# Create React App for SharePoint

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).  

This fork of Create React App is customized to work within a SharePoint Site Page. It enables you to create a fully custom single page React app independent of the default SharePoint UI.

## Features

- All features of [Create React App](https://github.com/facebookincubator/create-react-app).
- Polyfills added for IE 11 
- SharePoint environment build setup — `webpack.config.js` is customized to output files that can work within the SharePoint folder structure. 
- Sass enabled
- Typescript enabled
- Common SharePoint functions for getUser, getList, createListItem, updateListItem, and spLoad (load sharepoint) are included.
  
## Requirements
- node.js - 8.11+
- SharePoint site page for deployment

## Deployment Setup

`index.html` designed to be integrated into a Script editor on a site page.  All assets are set to be referenced from `SiteAssets/css/` & `SiteAssets/js/`.  

SharePoint content structure should be organized as follows. 

```
Site Contents
├─ Site Pages
│  ├─ Home.aspx
│  └─ Dev.aspx
└ Site Assets
   ├─ css
   │  ├─ [name][hash].css
   │  └─ [name]-dev.css
   └ js
      ├─ [name][hash].js
      └─ [name]-dev.js
```

Page names are flexible.  Asset naming, file configuration, and build settings can be managed in `config/webpack.config.js`.

## Important Notes
- `_sharepoint-reset.scss` - This file should reset most of the SharePoint UI components so the App can function as a stand alone page.  This will also hide the `Edit` and `Save` button in the SharePoint suitebar.  You should have a plan to make this visible again when you need to edit the page.

## Available Scripts

In the project directory, you can run:

### `npm install`

Installs all project dependencies.

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](#running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

### `npm run build-dev`

Builds the app for production on a development page to the `build` folder. It correctly bundles React in production mode and optimizes the build for the best performance.<br>

The build is minified ***WITHOUT*** hashes.<br>

This is useful if you would to integrate a development page that is private from the public page.  Since assets are not hashed, you only need to update the css or js in order to see your updates.

## More information
Read the [Create React App](https://github.com/facebookincubator/create-react-app) documentation for more information about how this app works.  