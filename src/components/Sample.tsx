import "../styles/modules/sample.scss";
import React, { useState, useEffect } from "react";
import Button from "./Button";

export default function Sample(props) {
  let name = props.user.title && "User: " + props.user.title;
  let permissions = props.user.isManager
    ? "You are a Site Manager"
    : "You are a Site Reader";

  return (
    <div className="sample">
      <h4>This is a sample React page in SharePoint</h4>
      <p>{name || "Loading..."}</p>
      <p>{name && permissions}</p>
      {props.user.isManager && (
        <Button
          onClick={() => {
            document.body.classList.toggle("show-sharepoint");
          }}
        >
          Toggle SharePoint Suite Bar
        </Button>
      )}
    </div>
  );
}
