import React, { Component } from "react";
import "../styles/modules/button.scss";
import classNames from "classnames";

class Button extends Component<any> {
  constructor(props: any) {
    super(props);
  }

  render() {
    return (
      <a {...this.props} className={classNames({
        "button": true,
        "button--tiny": this.props.tiny,
        "button--small": this.props.small,
        "button--large": this.props.large,
        "button--secondary": this.props.secondary
      })}>
        {this.props.children}
      </a>
    );
  }
}

export default Button;
