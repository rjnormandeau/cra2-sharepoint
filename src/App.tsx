import React, { useState, useEffect } from "react";
import logo from "./logo.svg";
import "./styles/all.scss";
import Sample from "./components/Sample";
import { spLoad } from "./utility/spLoad";
import { spGetUser } from "./utility/spGetUser";

function App() {
  
  // states
  let [user, setUser] = useState({title:null, isManager:null});

  // on mount
  useEffect(() => {
    spLoad();
    spGetUser(setUser);
  }, []);

  return (
    <div className="App">
      <Sample user={user} />
    </div>
  )
}

export default App;
