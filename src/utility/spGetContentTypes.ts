declare var SP: any;
let spContentTypes: any[] = [];

/**
 * Retrieve content type data from SharePoint
 */ 
function spGetContentTypes(listTitle, callback) {
  // console.log('Retrieving ContentTypes...');
  //Get the current context
  const context = new SP.ClientContext();

  //Get references to Lists and Libraries
  const contentTypes = context.get_web().get_lists().getByTitle( listTitle ).get_contentTypes();

  //Call SharePoint for the data
  context.load(contentTypes);

  // Go fetch the data!
  context.executeQueryAsync( () => {
    let contentTypesEnum = contentTypes.getEnumerator();
    spRenderContentTypes(contentTypesEnum, callback)
  });
}

// Render Content Types Enumerator to json; save to `contentTypes`
const spRenderContentTypes = (contentTypesEnum, callback)=>{
  while (contentTypesEnum.moveNext()) {
    let currentRecord = contentTypesEnum.get_current();
    // console.log(currentRecord);

    let item = {
      name: currentRecord.get_name(),
      id: currentRecord.get_id().get_stringValue()
    };
    spContentTypes.push(item);
  }
  // console.log("Content Types: ", spContentTypes);
  callback();
}

// Get Content Type by ID
const spLookupContentType = (id) =>{
  let contentTypeId = spContentTypes.find(
    e=>id.indexOf(e.id)>-1
  )
  return contentTypeId.name
}


export { spGetContentTypes, spLookupContentType, spContentTypes  }
