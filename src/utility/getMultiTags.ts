function getMultiTags(fieldObject: any, itemType?: string) {
  var tagArray: any[] = [];

  // if item is a lookup field array
  if ( itemType === "lookup") {
    fieldObject.forEach(element => {
      tagArray.push(element.get_lookupValue().toString());
    });
  } else {
    var tagEnumerator = fieldObject.getEnumerator();
    while (tagEnumerator.moveNext()) {
      var currentTerm = tagEnumerator.get_current();
      var label = currentTerm.get_label();
      tagArray.push(label);
    }
  }
  // remove dupes
  return tagArray.filter((item, pos, array) => {
    return array.indexOf(item) == pos;
  });
}

export { getMultiTags };
