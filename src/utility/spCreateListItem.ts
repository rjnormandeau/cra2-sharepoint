

declare var SP: any;

// Check to see if SP is loaded before getting data
/**  Create a List Item
 * where data is an array [ {column: columnsName, data: dataValue} ] **/
const spCreateListItem = (list:string, data: any[], callback?) => {
  if (window.SPisLoaded) {
    spCreateListItemOnLoad(list, data, callback);
  } else {
    setTimeout(function() {
      spCreateListItem(list, data, callback);
    }, 100);
  }
};
const spCreateListItemOnLoad = (list, data, callback) => {
  console.log("adding item...");
  //Get the current context
  const clientContext = new SP.ClientContext();

  //Get references to Lists and Libraries
  const oList = clientContext
    .get_web()
    .get_lists()
    .getByTitle(list);
  // Create new item
  const itemCreateInfo = new SP.ListItemCreationInformation();
  const oListItem = oList.addItem(itemCreateInfo);
  // add columns
  data.forEach(i => {
    oListItem.set_item(i.column, i.data);
  });
  // update listItem
  oListItem.update();
  clientContext.load(oListItem);
  clientContext.executeQueryAsync(
    function(sender, args) {
      onQuerySucceeded(args);
      callback && callback("success");
    },
    function(sender, args) {
      onQueryFailed(args);
      callback && callback("error");
    }
  );
};

function onQuerySucceeded(args) {
  console.log("Success");
}

function onQueryFailed(args) {
  console.log(
    "Request failed. " + args.get_message() + "\n" + args.get_stackTrace()
  );
}

export {spCreateListItem} ;
