// This file loads all the necessary SharePoint libraries necessary to read and write to list and libraries.

declare var SP: any;

// set global variable to track if SP is loaded
declare global {
  interface Window {
    SPisLoaded: boolean;
  }
}
window.SPisLoaded = false;

// your SharePoint site _layout/15/ folder:
const scriptbase =
  "https://collab.pncint.net/sites/SS/PPMPortal/SitePages/_layouts/15/";

/**
 * Loads all the necessary SharePoint libraries necessary to read and write to list and libraries.
 */
function spLoad() {
  // DOMContentLoaded isn't needed with hooks
  // document.addEventListener("DOMContentLoaded", function() {
    // do not reference SP in development
    if (process.env.NODE_ENV === "development") {
      return;
    }
    getScript(scriptbase + "SP.Runtime.js", function () {
      getScript(scriptbase + "SP.js", function () {
        getScript(scriptbase + "SP.Taxonomy.js", init);
      });
    });
  // });
}

// initial functions to run once SharePoint is loaded
const init = () => {
  // content types need to be loaded first
  console.log("SharePoint is Loaded");
  // set global variable so other functions know if SP is loaded
  window.SPisLoaded = true;
};

// jQuery.getScript alternative
function getScript(source, callback) {
  var script: any = document.createElement("script");
  var prior: any = document.getElementsByTagName("script")[0];
  script.async = true;

  script.onload = script.onreadystatechange = function(
    _: GlobalEventHandlers,
    isAbort: Event
  ) {
    if (
      isAbort ||
      !script.readyState ||
      /loaded|complete/.test(script.readyState)
    ) {
      script.onload = script.onreadystatechange = null;
      script = undefined;

      if (!isAbort) {
        if (callback) callback();
      }
    }
  };
  script.src = source;
  prior.parentNode.insertBefore(script, prior);
}

export { spLoad };
