// Update a List Item
// where data is an array [ {column: name, data: data} ]

declare var SP: any;

// Check to see if SP is loaded before getting data
const spUpdateListItem = (
  list: string,
  id: number,
  data: any[],
  callback?: Function
) => {
  if (window.SPisLoaded) {
    spUpdateListItemOnLoad(list, id, data, callback);
  } else {
    setTimeout(function() {
      spUpdateListItem(list, id, data, callback);
    }, 100);
  }
};
const spUpdateListItemOnLoad = (list, id, data, callback) => {
  // console.log("updating item...", list, id, data);
  //Get the current context
  const clientContext = new SP.ClientContext();

  //Get references to Lists and Libraries
  const oList = clientContext
    .get_web()
    .get_lists()
    .getByTitle(list);
  // set item
  const oListItem = oList.getItemById(id);
  // add columns
  data.forEach(i => {
    oListItem.set_item(i.column, i.data);
  });
  // update listItem
  oListItem.update();
  clientContext.load(oListItem);
  clientContext.executeQueryAsync(
    function(sender, args) {
      onQuerySucceeded(args);
      callback && callback("success");
    },
    function(sender, args) {
      onQueryFailed(args);
      callback && callback("error");
    }
  );
};

function onQuerySucceeded(args) {
  console.log("Success");
}

function onQueryFailed(args) {
  console.log(
    "Request failed. " + args.get_message() + "\n" + args.get_stackTrace()
  );
}

export {spUpdateListItem};
