declare var SP: any;

/**
 * Retrieves user information from SharePoint and return a callback with the array
*/ 
function spGetUser(callback?: Function) {
  if (window.SPisLoaded) {
    spGetUserOnLoad(callback);
  } else {
    setTimeout(function() {
      spGetUser(callback);
    }, 100);
  }
};

// Get User info
function spGetUserOnLoad(callback?: Function){
  console.log("Retrieving User...");
  //Get the current context
  const context = new SP.ClientContext();

  // Get references to User and SP site to test for permissions
  const user = context.get_web().get_currentUser();
  const permission = context
    .get_web()

  //Call SharePoint for the data
  context.load(user);
  context.load(permission, "EffectiveBasePermissions");

  // Go fetch the data!
  context.executeQueryAsync(() => {
    renderUser(user, permission, callback);
  }, onQueryFailed);
};

// Render user info to json; return in callback
function renderUser(user, permission, callback) {
  // console.log("user", user, permission);
  let item = {
    title: user.get_title(),
    email: user.get_email(),
    // groupsName: user.get_groups().getByName(),
    login: user.get_loginName(),
    // id: user.get_id(),
    // userId: user.get_userId(),
    isManager: permission
      .get_effectiveBasePermissions()
      .has(SP.PermissionKind.editListItems)
  };
  // window.dataLayer = window.dataLayer || [];
  // dataLayer.push({ userID: item.login });

  console.log("User:", item);

  // optional callback function...
  if (callback && typeof callback === "function") {
    callback(item);
  }
};

function onQueryFailed(args) {
  console.log(
    "Request failed. " + args.get_message() + "\n" + args.get_stackTrace()
  );
}

export { spGetUser };
