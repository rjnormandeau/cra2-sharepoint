import { spGetContentTypes, spLookupContentType } from "./spGetContentTypes";
import { getMultiTags } from "./getMultiTags";

declare var SP: any;

// Prefixes are added to each item ID so they are unique to the list/library
let idPrefixes = {
  "Content Library": "CL",
  "Document Library": "DL",
  "Site Data": "SD"
}


/** Retrieve a list/library from SharePoint and return a callback with the array or items. */
function spGetList(listTitle: string, callback: Function, camlQuery?: string) {
  // Check to see if SP is loaded before getting data
  if (window.SPisLoaded) {
    // get content types then proceed
    spGetContentTypes(listTitle, () => {
      spGetListWithSP(listTitle, callback, camlQuery);
    });
  } else {
    setTimeout(function() {
      spGetList(listTitle, callback, camlQuery);
    }, 100);
  }
}

// Get List
const spGetListWithSP = (listTitle, callback, camlQuery) => {
  console.log("Retrieving", listTitle, "...");
  //Get the current context
  const context = new SP.ClientContext();

  //Get references to Lists and Libraries
  const docList = context
    .get_web()
    .get_lists()
    .getByTitle(listTitle);

  //Create a new CAML query object
  const docListCAML = new SP.CamlQuery();

  //If there is a query string defined, then pass it to the CAML object. No string means return everything.
  if (camlQuery) {
    // console.log(listTitle, "query:", camlQuery )
    docListCAML.set_viewXml(camlQuery);
  }

  //Apply CAML query to each list
  const docListResults = docList.getItems(docListCAML);

  //Call SharePoint for the data
  context.load(docListResults);

  // Go fetch the data!
  context.executeQueryAsync(() => {
    let listEnum = docListResults.getEnumerator();
    renderList(listTitle, listEnum, callback);
  }, onQueryFailed);
}; // end spGetList

// Render list items to json; return in callback
const renderList = (listTitle, listEnum, callback) => {
  let listIndex: any[] = [];

  while (listEnum.moveNext()) {
    let currentRecord = listEnum.get_current();
    // retrieve all field values
    let fields = currentRecord.get_fieldValues();

    let item = {
      title: currentRecord.get_item("Title"),
      list: listTitle,
      contentType: spLookupContentType(  currentRecord.get_item("ContentTypeId").get_stringValue()  ),
      id: idPrefixes[listTitle]+currentRecord.get_item("ID"),
      tags: fields.Tags && getMultiTags(currentRecord.get_item("Tags")),    
    };

    listIndex.push(item);

    // if(item.list === "Site Data") console.log("Current", item.title,": ",currentRecord);
    // console.log(item.relatedTemplates, item.relatedTasks)
    // console.log("Field Values:", fields);
  }

  // console.log(listTitle, "success:", listIndex);
  // optional callback function...
  callback(listTitle, listIndex);
};

function onQueryFailed(args) {
  console.log(
    "Request failed. ", args,
    // args.get_message() + "\n" + args.get_stackTrace()
  );
}

export { spGetList };
